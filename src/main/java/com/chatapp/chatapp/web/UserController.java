package com.chatapp.chatapp.web;

import com.chatapp.chatapp.domain.User;
import com.chatapp.chatapp.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@CrossOrigin("*")
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public User register(@Valid @RequestBody User user){
        return userService.save(user);
    }

    @GetMapping
    public List<User> findAll(){
        return userService.findAll();
    }

    @GetMapping("/{name}")
    public User findSingle(@PathVariable String name){
        return userService.findUserByUsername(name);
    }

    @GetMapping("/online")
    public List<User> findAllOnlineUsers(){
        return userService.findOnlineUsers();
    }




}
