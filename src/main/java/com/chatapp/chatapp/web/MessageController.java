package com.chatapp.chatapp.web;

import com.chatapp.chatapp.domain.ChatMessage;
import com.chatapp.chatapp.domain.service.ChatMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
public class MessageController {

    @Autowired
    private ChatMessageService service;

    @MessageMapping("/sendMessage")
    public void sendChatMessage(@Payload ChatMessage message){
        service.sendMessage(message);

    }
    @MessageMapping("/joinChat")
    public void sendUserJoinedMessage(@Payload String joinedUser, SimpMessageHeaderAccessor headerAccessor){
        headerAccessor.getSessionAttributes().put("username", joinedUser);
        String chatMessageType = ChatMessage.MessageType.CONNECTED.name();
        service.sendUserConnectionStatusMessage(joinedUser,chatMessageType);
    }



}
