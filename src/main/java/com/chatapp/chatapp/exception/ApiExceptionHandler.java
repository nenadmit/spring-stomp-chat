package com.chatapp.chatapp.exception;

import org.apache.coyote.Response;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;

@Service
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    return ResponseEntity.status(HttpStatus.CONFLICT)
            .body(new ApiError(ex.getBindingResult().getFieldErrors().get(0).getDefaultMessage(), HttpStatus.CONFLICT));
  }

  @ExceptionHandler(ConflictException.class)
  public ResponseEntity<ApiError> handle(ConflictException ex) {
    return ResponseEntity.status(HttpStatus.CONFLICT)
        .body(new ApiError(ex.getMessage(), HttpStatus.CONFLICT));
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ApiError> handle(EntityNotFoundException ex) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
        .body(new ApiError(ex.getMessage(), HttpStatus.NOT_FOUND));
  }
}
