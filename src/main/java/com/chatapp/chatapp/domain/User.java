package com.chatapp.chatapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Size(min = 5, message = "Username cannot be less than 5 characters.")
  @NotBlank(message = "Username cannot be blank")
  @NotNull(message = "Username cannot be null")
  private String username;

  @JsonIgnore
  @OneToMany(mappedBy = "sender")
  private Set<ChatMessage> sentMessages;

  private String status = UserOnlineStatus.OFFLINE.name();

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Set<ChatMessage> getSentMessages() {
    return sentMessages;
  }

  public void setSentMessages(Set<ChatMessage> sentMessages) {
    this.sentMessages = sentMessages;
  }

  public enum UserOnlineStatus{
    ONLINE,OFFLINE
  }
}
