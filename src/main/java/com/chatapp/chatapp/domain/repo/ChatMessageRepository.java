package com.chatapp.chatapp.domain.repo;

import com.chatapp.chatapp.domain.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatMessageRepository extends JpaRepository<ChatMessage,Long> {

}
