package com.chatapp.chatapp.domain.service;

import com.chatapp.chatapp.domain.User;
import com.chatapp.chatapp.domain.repo.UserRepository;
import com.chatapp.chatapp.exception.ConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

  private UserRepository userRepository;

  @Autowired
  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public synchronized User save(User user) {

    if (userRepository.findByUsername(user.getUsername()).isPresent()) {
      throw new ConflictException("Username already present");
    }

    return userRepository.save(user);
  }

  public List<User> findAll() {
    return userRepository.findAll();
  }

  public User findUserByUsername(String username) {
    Optional<User> optional = userRepository.findByUsername(username);
    if (!optional.isPresent()) {
      throw new EntityNotFoundException("User not found " + username);
    }
    return optional.get();
  }

  public List<User> findOnlineUsers() {
    return userRepository.findAllByStatus(User.UserOnlineStatus.ONLINE.name());
  }

  public User updateUserStatus(User user, String status) {
    user.setStatus(status);
    return userRepository.save(user);
  }
}
