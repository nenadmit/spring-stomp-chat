package com.chatapp.chatapp.domain.service;

import com.chatapp.chatapp.domain.ChatMessage;
import com.chatapp.chatapp.domain.User;
import com.chatapp.chatapp.domain.repo.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatMessageService {

  private SimpMessagingTemplate messagingTemplate;
  private ChatMessageRepository chatMessageRepository;
  private UserService userService;

  @Autowired
  public ChatMessageService(
      SimpMessagingTemplate messagingTemplate,
      ChatMessageRepository chatMessageRepository,
      UserService userService) {
    this.messagingTemplate = messagingTemplate;
    this.chatMessageRepository = chatMessageRepository;
    this.userService = userService;
  }

  public void sendMessage(ChatMessage chatMessage) {

    chatMessage.setSender(userService.findUserByUsername(chatMessage.getSenderName()));
    chatMessage.setType(ChatMessage.MessageType.REGULAR.name());

    messagingTemplate.convertAndSend("/topic/public", chatMessage);
  }

  public void sendUserConnectionStatusMessage(String joinedUser,String messageType){

    User user = userService.findUserByUsername(joinedUser);

    if(messageType.equals(ChatMessage.MessageType.DISCONNECTED.name())){
      userService.updateUserStatus(user,User.UserOnlineStatus.OFFLINE.name());
    }
    if(messageType.equals(ChatMessage.MessageType.CONNECTED.name())){
      userService.updateUserStatus(user,User.UserOnlineStatus.ONLINE.name());
    }

    ChatMessage chatMessage = new ChatMessage();
    chatMessage.setType(messageType);
    chatMessage.setSender(user);

    messagingTemplate.convertAndSend("/topic/public",chatMessage);
  }

  public List<ChatMessage> findAll() {
    return chatMessageRepository.findAll();
  }
}
