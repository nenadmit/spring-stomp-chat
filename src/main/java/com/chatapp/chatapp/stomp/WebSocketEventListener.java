package com.chatapp.chatapp.stomp;

import com.chatapp.chatapp.domain.ChatMessage;
import com.chatapp.chatapp.domain.service.ChatMessageService;
import com.chatapp.chatapp.domain.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class WebSocketEventListener {

  private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

  @Autowired
  private ChatMessageService messageService;

  @EventListener
  public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
    StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
    String username = (String) headerAccessor.getSessionAttributes().get("username");
    messageService.sendUserConnectionStatusMessage(
        username, ChatMessage.MessageType.DISCONNECTED.name());

    logger.info("User has disconnected " + username);
  }
}
