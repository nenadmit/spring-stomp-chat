const url = 'http://localhost:8081';
let stompClient;
var usernameForm = document.querySelector('#usernameForm');
var chatMessageBox = document.querySelector('#chatMessageBox');
var sendButton = document.querySelector('#sendButton');
var chatBox = document.querySelector('#chat');
var availableUsers = document.querySelector('#availableUsers');
let onlineUsers = [];
let loggedInUser;


async function joinChat(event){

    event.preventDefault();

    var username = document.querySelector('#username').value;
   
    await axios.get(url +'/users/' + username)
    .then(response => {
        loggedInUser = response.data;
    }).catch(error=>{
        axios.post(url+'/users',{username:username})
        .then(response => {
            loggedInUser = response.data
            alert('User not found '+ username +'. User has been registered. Try joining again.');
        })

    })

    if(loggedInUser){
        document.querySelector('#username-page').classList.add('hidden');
        document.querySelector('#container').classList.remove('hidden');
        updateChatHeader(loggedInUser);

        var sock = new SockJS(url+'/chat-app')
        stompClient = Stomp.over(sock);
        stompClient.connect({},onConnect,{});
    }
   

}

function onConnect(){
    

    stompClient.subscribe('/topic/public',appendMessageBox)
    
    stompClient.send("/app/joinChat",
    {},loggedInUser.username);
}

function sendMessage(event){
    event.preventDefault();
    let message = chatMessageBox.value;

    let messageOne = {
        content:message,
        senderName:loggedInUser.username
    }

    stompClient.send('/app/sendMessage',{},JSON.stringify(messageOne));

    chatMessageBox.value = '';
}

function appendMessageBox(payload){

    var message = JSON.parse(payload.body);

    var chatMessage = document.createElement('li');
    var statusMessage = document.createElement('h3');
    var {username} = message.sender;
    console.log(message.type);

    if(message.type === 'CONNECTED'){
    
        let li = document.createElement('li');
        statusMessage.innerHTML = username + ' has connected to the chat!';
        li.appendChild(statusMessage);
        chatBox.appendChild(li);
        fetchAllOnlineUsers()
    }else if(message.type === 'DISCONNECTED'){
        let lii = document.createElement('li');
        statusMessage.innerHTML = username + ' has disconnected from the chat!';
        lii.appendChild(statusMessage);
        chatBox.appendChild(lii);
        fetchAllOnlineUsers()
    }else{

        let chatMessage = document.createElement('li');
        let userInfoDiv = document.createElement('div');
        userInfoDiv.classList.add('entete');
        let usernameH2 = document.createElement('h2');
        usernameH2.innerHTML = message.sender.username;
        let dateH3 = document.createElement('h3');
        dateH3.innerHTML = message.created;
        let colorSpan = document.createElement('span');
    
        if(message.sender.username === loggedInUser.username){
            chatMessage.classList.add('me')
            colorSpan.classList.add('status','blue');
            userInfoDiv.appendChild(dateH3);
            userInfoDiv.appendChild(usernameH2);
            userInfoDiv.appendChild(colorSpan);
        }else{
            chatMessage.classList.add('you')
            colorSpan.classList.add('status','green');
            userInfoDiv.appendChild(colorSpan);
            userInfoDiv.appendChild(usernameH2);
            userInfoDiv.appendChild(dateH3);
        }
        let triangleDiv = document.createElement('div');
        triangleDiv.classList.add('triangle');
    
        let messageContainerDiv = document.createElement('div');
        messageContainerDiv.classList.add('message');
        messageContainerDiv.innerHTML = message.content;
    
        chatMessage.appendChild(userInfoDiv);
        chatMessage.appendChild(triangleDiv);
        chatMessage.appendChild(messageContainerDiv);

        chatBox.appendChild(chatMessage);
    
    }
    
}


function fetchAllOnlineUsers(){

    availableUsers.innerHTML = '';
    $('#avaliableUsers').empty();

    axios.get(url +'/users/online')
    .then(response => {
        onlineUsers = response.data;
        onlineUsers.map(user => addConnectedUser(user));
    })

}

function addConnectedUser(sender){

  var newLi = document.createElement('li');
  var newDiv = document.createElement('div');
  var h2 = document.createElement('h2');
  h2.id = sender.id;
  h2.appendChild(document.createTextNode(sender.username));

  var img = document.createElement("img");
  img.src = 'https://robohash.org/'+ sender.id +'?size=50x50';

  newLi.appendChild(img);
  newDiv.appendChild(h2);

  var h3 = document.createElement('h3');
  var newSpan = document.createElement('span');
  newSpan.classList.add("status", "green");

  h3.appendChild(newSpan);
  h3.appendChild(document.createTextNode("online"));
  newDiv.appendChild(h3);

  newLi.appendChild(newDiv);  
  availableUsers.appendChild(newLi);

}

function removeConnectedUser(user){

    document.querySelector('#'+user.id).remove();
}

function updateChatHeader(user){

    selectedUser = user;
    var chatProfile = document.querySelector("#chatProfile")
    chatProfile.src = 'https://robohash.org/'+ user.id +'?size=50x50'
    var chatName = document.querySelector("#chatName");
    chatName.innerHTML = "Welcome " + user.username + '!';
    var userStatus = document.querySelector('#userStatus');
    userStatus.innerHTML = 'Online'
  
  }

sendButton.addEventListener('click',sendMessage,true);
usernameForm.addEventListener('submit',joinChat,true);

